from random import randint

length = 10 # longueur du mdp
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
v = ['a','e','i','o','u','y']
c = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z']
lastletter = ''
mdpass = ''

for x in range(0, length):
    if x < length - 2:
        if lastletter in v:
            l = c[randint(0, 19)]
        elif lastletter in c:
            l = v[randint(0, 5)]
        else:
            l = alphabet[randint(0, 19)]
        mdpass = mdpass + l
        lastletter = l
    else:
        l = str(randint(0, 9))
        mdpass = mdpass + l
    

print(mdpass)